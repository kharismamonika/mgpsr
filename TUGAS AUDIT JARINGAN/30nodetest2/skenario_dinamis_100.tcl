# ======================================================================
# Default Script Options
# ======================================================================
set opt(chan)		Channel/WirelessChannel
set opt(prop)		Propagation/TwoRayGround
set opt(netif)		Phy/WirelessPhy
set opt(mac)		Mac/802_11
set opt(ifq)		Queue/DropTail/PriQueue	;# for dsdv
set opt(ll)			LL
set opt(ant)        Antenna/OmniAntenna
set opt(x)			1000		;# X dimension of the topography
set opt(y)			1000		;# Y dimension of the topography
set opt(ifqlen)		1000		;# max packet in ifq
set opt(nn)			30		;# number of nodes
set opt(seed)		0.0
set opt(adhocRouting)         gpsr		;# routing protocol script (dsr or dsdv)
set opt(stop)		200.0		;# simulation time
set opt(cp)			"traffic"			;#<-- traffic file
set opt(sc)			"mobility"			;#<-- mobility file 
# ======================================================================

# The transimssion radio range 
#Phy/WirelessPhy set Pt_ 6.9872e-4    ;# ?m
#Phy/WirelessPhy set Pt_ 8.5872e-4    ;# 40m
#Phy/WirelessPhy set Pt_ 1.33826e-3   ;# 50m
#Phy/WirelessPhy set Pt_ 7.214e-3     ;# 100m
Phy/WirelessPhy set Pt_ 0.2818       ;# 250m
# ======================================================================

# # 802.11p default parameters
Phy/WirelessPhy	set	RXThresh_ 5.57189e-11 ; #400m
Phy/WirelessPhy set	CSThresh_ 5.57189e-11 ; #400m

# Agent/GPSR setting
Agent/GPSR set planar_type_  1   ;#1=GG planarize, 0=RNG planarize
Agent/GPSR set hello_period_   0.1 ;#Hello message period

# ======================================================================

#
# Initialize Global Variables
#
set ns_		[new Simulator]
set chan	[new $opt(chan)]
set prop	[new $opt(prop)]
set tracefd	[open sd100.tr w]
set namtrace    [open sd100.nam w]

$ns_ trace-all $tracefd
$ns_ namtrace-all-wireless $namtrace $opt(x) $opt(y)


set topo	[new Topography]
$topo load_flatgrid $opt(x) $opt(y)


#
# Create God
#
set god_ [create-god $opt(nn)]

#global node setting
$ns_ node-config -adhocRouting $opt(adhocRouting) \
                 -llType $opt(ll) \
                 -macType $opt(mac) \
                 -ifqType $opt(ifq) \
                 -ifqLen $opt(ifqlen) \
                 -antType $opt(ant) \
                 -propType $opt(prop) \
                 -phyType $opt(netif) \
		 -channelType $opt(chan) \
		 -topoInstance $topo \
		 -agentTrace ON \
		 -routerTrace ON \
		 -macTrace ON \
		 -movementTrace ON \
								 
###
source ~/Downloads/ns-allinone-2.35/ns-2.35/gpsr/gpsr.tcl

for {set i 0} {$i < $opt(nn) } {incr i} {
	gpsr-create-mobile-node $i
	$node_($i) namattach $namtrace
}


# Define node movement model
puts "Loading connection pattern..."
source $opt(cp)

# Define traffic model
puts "Loading scenario file..."
source $opt(sc)

# Define node initial position in nam

for {set i 0} {$i < $opt(nn)} {incr i} {

    # 20 defines the node size in nam, must adjust it according to your scenario
    # The function must be called after mobility model is defined
    
    $ns_ initial_node_pos $node_($i) 100
    $ns_ color $node_($i) blue
}

# Tell nodes when the simulation ends
for {set i 0} {$i < $opt(nn) } {incr i} {
    $ns_ at $opt(stop).0 "$node_($i) reset";
}
#$ns_ at  $opt(stop)	"stop"
$ns_ at  $opt(stop).0002 "puts \"NS EXITING...\" ; $ns_ halt"

puts $tracefd "M 0.0 nn $opt(nn) x $opt(x) y $opt(y) rp $opt(adhocRouting)"
puts $tracefd "M 0.0 sc $opt(sc) cp $opt(cp) seed $opt(seed)"
puts $tracefd "M 0.0 prop $opt(prop) ant $opt(ant)"

puts "Starting Simulation..."
$ns_ run
