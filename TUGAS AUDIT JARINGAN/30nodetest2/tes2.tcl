# ======================================================================
# Define options
# ======================================================================

set opt(chan)       		Channel/WirelessChannel		;# channel type
set opt(prop)       		Propagation/TwoRayGround	;# radio-propagation model
set opt(netif)      		Phy/WirelessPhy			;# network interface type
set opt(mac)        		Mac/802_11			;# MAC type
set opt(ifq)        		Queue/DropTail/PriQueue		;# interface queue type
set opt(ll)         		LL				;# link layer type
set opt(ant)        		Antenna/OmniAntenna		;# antenna model
set opt(x)              	1000   				;# X dimension of the topography
set opt(y)              	1000   				;# Y dimension of the topography
set opt(ifqlen)         	1000				;# max packet in ifq
set opt(nn)             	30				;# how many nodes are simulated
set opt(seed)				0.0				;
set opt(adhocRouting)   	gpsr				;# routing protocol
set opt(stop)           	200				;# simulation time
set opt(cp)			"traffic"			;#<-- traffic file
set opt(sc)			"mobility"			;#<-- mobility file 

# Agent/GPSR setting
Agent/GPSR set planar_type_  1   ;#1=GG planarize, 0=RNG planarize
Agent/GPSR set hello_period_   1.0 ;#Hello message period



# =====================================================================
# Main Program
# ======================================================================
# Initialize Global Variables
# create simulator instance

set ns_		[new Simulator]
set chan    [new $opt(chan)]
set prop    [new $opt(prop)]

# setup topography object

set topo	[new Topography]

#if { $opt(adhocRouting) == "DSR" } {
#set opt(ifq)            CMUPriQueue
#} else {
#set opt(ifq)            Queue/DropTail/PriQueue
#}
# create trace object for ns and nam

set tracefd	[open tes2.tr w]
set namtrace    [open tes2.nam w]

$ns_ trace-all $tracefd
$ns_ namtrace-all-wireless $namtrace $opt(x) $opt(y)

# set up topology object
set topo				[new Topography]
$topo load_flatgrid $opt(x) $opt(y)

# Create God
set god_ [create-god $opt(nn)]

#global node setting
$ns_ node-config -adhocRouting $opt(adhocRouting) \
                 -llType $opt(ll) \
                 -macType $opt(mac) \
                 -ifqType $opt(ifq) \
                 -ifqLen $opt(ifqlen) \
                 -antType $opt(ant) \
                 -propType $opt(prop) \
                 -phyType $opt(netif) \
		 -channelType $opt(chan) \
		 -topoInstance $topo \
		 -agentTrace ON \
		 -routerTrace ON \
		 -macTrace ON \
		 -movementTrace ON \
								 
###

# 802.11p default parameters
Phy/WirelessPhy	set	RXThresh_ 5.57189e-11 ; #400m
Phy/WirelessPhy set	CSThresh_ 5.57189e-11 ; #400m

# ga perlu diganti
source ~/Downloads/ns-allinone-2.35/ns-2.35/gpsr/gpsr.tcl

###
#  Create the specified number of nodes [$opt(nn)] and "attach" them
#  to the channel. 
for {set i 0} {$i < $opt(nn) } {incr i} {
gpsr-create-mobile-node $i
$node_($i) namattach $namtrace 
#This command is used to attach the namtrace file descriptor <namtracefd> to the mobilenode. All nam traces for the node are then written into this namtrace file.
}

# Define node movement model
puts "Loading connection pattern..."
source $opt(cp)

# Define traffic model
puts "Loading scenario file..."
source $opt(sc)


# Define node initial position in nam

for {set i 0} {$i < $opt(nn)} {incr i} {

    # 20 defines the node size in nam, must adjust it according to your scenario
    # The function must be called after mobility model is defined
    
    $ns_ initial_node_pos $node_($i) 100
    $ns_ color $node_($i) blue
}

# Tell nodes when the simulation ends
for {set i 0} {$i < $opt(nn) } {incr i} {
    $ns_ at $opt(stop).0 "$node_($i) reset";
}

#$ns_ at  $opt(stop)	"stop"
$ns_ at  $opt(stop).0002 "puts \"NS EXITING...\" ; $ns_ halt"

puts $tracefd "M 0.0 nn $opt(nn) x $opt(x) y $opt(y) rp $opt(adhocRouting)"
puts $tracefd "M 0.0 sc $opt(sc) cp $opt(cp) seed $opt(seed)"
puts $tracefd "M 0.0 prop $opt(prop) ant $opt(ant)"

puts "Starting Simulation..."
$ns_ run
