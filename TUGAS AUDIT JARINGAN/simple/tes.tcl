# ======================================================================
# Default Script Options
# ======================================================================
set opt(chan)		Channel/WirelessChannel
set opt(prop)		Propagation/TwoRayGround
set opt(netif)		Phy/WirelessPhy
set opt(mac)		Mac/802_11
set opt(ifq)		Queue/DropTail/PriQueue	;# for dsdv
set opt(ll)		LL
set opt(ant)            Antenna/OmniAntenna

set opt(x)		1000		;# X dimension of the topography
set opt(y)		1000		;# Y dimension of the topography
# File CBR dan mobility
set val(cp)		"trafic" 
set val(sc)		"dsdv-update.tcl"

set opt(ifqlen)		1000		;# max packet in ifq
set opt(nn)			30		;# number of nodes
set opt(seed)		1.0
set opt(stop)		200.0		;# simulation time
#set opt(tr)		trace.tr		;# trace file
#set opt(nam)            nam.out.tr
set opt(rp)             gpsr		;# routing protocol script (dsr or dsdv)
#set opt(lm)             "off"		;# log movement



# set val(chan)       		Channel/WirelessChannel		;# channel type
# set val(prop)       		Propagation/TwoRayGround	;# radio-propagation model
# set val(netif)      		Phy/WirelessPhy			;# network interface type
# set val(mac)        		Mac/802_11			;# MAC type
# set val(ifq)        		Queue/DropTail/PriQueue		;# interface queue type
# set val(ll)         		LL				;# link layer type
# set val(ant)        		Antenna/OmniAntenna		;# antenna model
# set opt(x)              	1000   				;# X dimension of the topography
# set opt(y)              	1000   				;# Y dimension of the topography
# set val(ifqlen)         	1000				;# max packet in ifq
# set val(nn)             	30				;# how many nodes are simulated
# set val(seed)				1.0				;
# set val(adhocRouting)   	gpsr			;# routing protocol
# set val(stop)           	200				;# simulation time
# set val(cp)			"trafic"			;#<-- traffic file
# set val(sc)			"dsdv-update.tcl"			;#<-- mobility file 

# ======================================================================

# Agent/GPSR setting
Agent/GPSR set planar_type_  1   ;#1=GG planarize, 0=RNG planarize
Agent/GPSR set hello_period_   5.0 ;#Hello message period

# ======================================================================




# ======================================================================
# Main Program
# ======================================================================
#
# Initialize Global Variables
# create simulator instance

set ns_		[new Simulator]
set chan	[new $opt(chan)]
set prop	[new $opt(prop)]

# setup topography object

set topo	[new Topography]

set tracefd	[open 30node.tr w]
set namtrace    [open 30node.nam w]

$ns_ trace-all $tracefd
$ns_ namtrace-all-wireless $namtrace $opt(x) $opt(y)

# set up topology object
set topo				[new Topography]
$topo load_flatgrid $opt(x) $opt(y)

# Create God
set god_ [create-god $opt(nn)]


$ns_ node-config -adhocRouting gpsr \
                 -llType $opt(ll) \
                 -macType $opt(mac) \
                 -ifqType $opt(ifq) \
                 -ifqLen $opt(ifqlen) \
                 -antType $opt(ant) \
                 -propType $opt(prop) \
                 -phyType $opt(netif) \
                 -channelType $opt(chan) \
                 -topoInstance $topo \
                 -agentTrace ON \
                 -routerTrace ON \
                 -macTrace ON \
                 -movementTrace ON 	 
###


# 802.11p default parameters
Phy/WirelessPhy	set	RXThresh_ 5.57189e-11 ; #400m
Phy/WirelessPhy set	CSThresh_ 5.57189e-11 ; #400m

# ga perlu diganti
source ~/Downloads/ns-allinone-2.35/ns-2.35/gpsr/gpsr.tcl

###
#  Create the specified number of nodes [$val(nn)] and "attach" them
#  to the channel. 
for {set i 0} {$i < $opt(nn) } {incr i} {
    gpsr-create-mobile-node $i
}


# Define node movement model
#
# Source the Connection and Movement scripts
#
if { $val(cp) == "" } {
	puts "*** NOTE: no connection pattern specified."
        set $val(cp) "none"
} else {
	puts "Loading connection pattern..."
	source $val(cp)
}


# Define traffic model
if { $val(sc) == "" } {
	puts "*** NOTE: no scenario file specified."
        set $val(sc) "none"
} else {
	puts "Loading scenario file..."
	source $val(sc)
	puts "Load complete..."
}

# Define node initial position in nam

for {set i 0} {$i < $opt(nn)} {incr i} {

    # 20 defines the node size in nam, must adjust it according to your scenario
    # The function must be called after mobility model is defined
    
    $ns_ initial_node_pos $node_($i) 100
}

# Tell nodes when the simulation ends
for {set i 0} {$i < $opt(nn) } {incr i} {
    $ns_ at $opt(stop).0 "$node_($i) reset";
}

#$ns_ at  $val(stop)	"stop"
$ns_ at  $opt(stop).0002 "puts \"NS EXITING...\" ; $ns_ halt"

puts $tracefd "M 0.0 nn $opt(nn) x $opt(x) y $opt(y) rp $opt(rp)"
puts $tracefd "M 0.0 sc $val(sc) cp $val(cp) seed $opt(seed)"
puts $tracefd "M 0.0 prop $opt(prop) ant $opt(ant)"

puts "Starting Simulation..."
$ns_ run
