# wrls1.tcl
# A 3-node example for ad-hoc simulation with DSDV

# Define options
set opt(chan)           Channel/WirelessChannel    ;# channel type
set opt(prop)           Propagation/TwoRayGround   ;# radio-propagation model
set opt(netif)          Phy/WirelessPhy            ;# network interface type
set opt(mac)            Mac/802_11                 ;# MAC type
set opt(ifq)            Queue/DropTail/PriQueue    ;# interface queue type
set opt(ll)             LL                         ;# link layer type
set opt(ant)            Antenna/OmniAntenna        ;# antenna model
set opt(ifqlen)         50                         ;# max packet in ifq
set opt(nn)             3                          ;# number of mobilenodes
set opt(rp)             gpsr                       ;# routing protocol
set opt(x)              500   			   ;# X dimension of topography
set opt(y)              400   			   ;# Y dimension of topography  
set opt(stop)			150			   ;# time of simulation end

# Agent/GPSR setting
Agent/GPSR set planar_type_  1   ;#1=GG planarize, 0=RNG planarize
Agent/GPSR set hello_period_   5.0 ;#Hello message period



set ns		  [new Simulator]
set tracefd       [open simple.tr w]
set windowVsTime2 [open win.tr w] 
set namtrace      [open simwrls.nam w] 
set chan	[new $opt(chan)]
set prop	[new $opt(prop)]   


$ns trace-all $tracefd
$ns namtrace-all-wireless $namtrace $opt(x) $opt(y)

# set up topography object
set topo       [new Topography]

$topo load_flatgrid $opt(x) $opt(y)

# ga perlu diganti
source ~/Downloads/ns-allinone-2.35/ns-2.35/gpsr/gpsr.tcl
source ~/Downloads/ns-allinone-2.35/ns-2.35/tcl/lib/ns-cmutrace.tcl
source ~/Downloads/ns-allinone-2.35/ns-2.35/tcl/lib/ns-bsnode.tcl
source ~/Downloads/ns-allinone-2.35/ns-2.35/tcl/mobility/com.tcl

create-god $opt(nn)



#
#  Create nn mobilenodes [$opt(nn)] and attach them to the channel. 
#

# configure the nodes
        $ns node-config -adhocRouting $opt(rp) \
			 -llType $opt(ll) \
			 -macType $opt(mac) \
			 -ifqType $opt(ifq) \
			 -ifqLen $opt(ifqlen) \
			 -antType $opt(ant) \
			 -propType $opt(prop) \
			 -phyType $opt(netif) \
			 -channelType $opt(chan) \
			 -topoInstance $topo \
			 -agentTrace ON \
			 -routerTrace ON \
			 -macTrace ON \
			 -movementTrace ON
			 
#  Create the specified number of nodes [$opt(nn)] and "attach" them
#  to the channel. 
for {set i 0} {$i < $opt(nn) } {incr i} {
    gpsr-create-mobile-node $i
}

# Provide initial location of mobilenodes
$node_(0) set X_ 1.0
$node_(0) set Y_ 240.0
$node_(0) set Z_ 0.0

$node_(1) set X_ 400.0
$node_(1) set Y_ 240.0
$node_(1) set Z_ 0.0

$node_(2) set X_ 200.0
$node_(2) set Y_ 240.0
$node_(2) set Z_ 0.0

# Generation of movements
$ns at 50.0 "$node_(0) setdest 250.0 250.0 3.0"
$ns at 65.0 "$node_(1) setdest 45.0 285.0 5.0"
$ns at 110.0 "$node_(0) setdest 480.0 300.0 5.0" 

# Set a TCP connection between node_(0) and node_(1)
# set tcp [new Agent/TCP/Newreno]
# $tcp set class_ 2
# set sink [new Agent/TCPSink]
# $ns attach-agent $node_(0) $tcp
# $ns attach-agent $node_(1) $sink
# $ns connect $tcp $sink
# set ftp [new Application/FTP]
# $ftp attach-agent $tcp
# $ns at 10.0 "$ftp start" 

# Printing the window size
# proc plotWindow {tcpSource file} {
# global ns
# set time 0.01
# set now [$ns now]
# set cwnd [$tcpSource set cwnd_]
# puts $file "$now $cwnd"
# $ns at [expr $now+$time] "plotWindow $tcpSource $file" }
# $ns at 10.1 "plotWindow $tcp $windowVsTime2"  

# # Define node initial position in nam.
# ukuran node
 for {set i 0} {$i < $opt(nn)} { incr i } {
# # 30 defines the node size for nam
 $ns initial_node_pos $node_($i) 30
 }

# Telling nodes when the simulation ends
for {set i 0} {$i < $opt(nn) } { incr i } {
    $ns at $opt(stop) "$node_($i) reset";
}


# ending nam and the simulation 
$ns at $opt(stop) "$ns nam-end-wireless $opt(stop)"
$ns at $opt(stop) "stop"
$ns at 150.00001 "puts \"end simulation\" ; $ns halt"



proc stop {} {
    global ns tracefd namtrace
    $ns flush-trace
    close $tracefd
    close $namtrace
}

$ns run