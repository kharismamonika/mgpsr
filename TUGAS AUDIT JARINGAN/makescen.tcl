# Define options
set val(chan)           Channel/WirelessChannel    ;# channel type
set val(prop)           Propagation/TwoRayGround   ;# radio-propagation model
set val(netif)          Phy/WirelessPhy            ;# network interface type
set val(mac)            Mac/802_11                 ;# MAC type
set val(ifq)            Queue/DropTail/PriQueue    ;# interface queue type
set val(ll)             LL                         ;# link layer type
set val(ant)            Antenna/OmniAntenna        ;# antenna model
set val(x)              500   			   ;# X dimension of topography
set val(y)              400   			   ;# Y dimension of topography  
set opt(cp)             "./gpsoCBRtest.tcl";
set opt(sc)				"./GPSOnodePos.tcl";
set val(ifqlen)         50                         ;# max packet in ifq
set val(nn)             3                          ;# number of mobilenodes
set opt(rp)				gpsr;		# routing protocol
set opt(seed)			0.0;
set val(stop)		150			   ;# time of simulation end
set opt(tr)				trace.tr;
set opt(nam)			nam.out.tr;
set opt(lm)				"off";

set ns		  [new Simulator]
#set tracefd       [open simple.tr w]
#set windowVsTime2 [open win.tr w] 
#set namtrace      [open simwrls.nam w]    