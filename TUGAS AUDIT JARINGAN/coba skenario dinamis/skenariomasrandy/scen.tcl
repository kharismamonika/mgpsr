set opt(chan)			Channel/WirelessChannel
set opt(prop)			Propagation/TwoRayGround
set opt(netif)			Phy/WirelessPhy
set opt(mac)			Mac/802_11
set opt(ifq)			Queue/DropTail/PriQueue;
set opt(ll)				LL
set opt(ant)            Antenna/OmniAntenna

set opt(x)				1000;
set opt(y)				1000;
set opt(cp)			"traffic1.tcl";
set opt(sc)			"mobility";

set opt(ifqlen)			512	;
set opt(nn)				30	;
set opt(seed)			0.0 ;
set opt(stop)			70.0;
set opt(tr)				"60_to_81.tr"	;
set opt(nam)            "60_to_81.nam";
set opt(rp)             GPSR;
set opt(lm)             "off";
# Agent/GPSR setting
Agent/GPSR set planar_type_  1  ;#1=GG planarize, 0=RNG planarize
Agent/GPSR set hello_period_   2.0 ;#Hello message period

proc usage { argv0 }  {
	puts "Usage: $argv0"
	puts "\tmandatory arguments:"
	puts "\t\t\[-x MAXX\] \[-y MAXY\]"
	puts "\toptional arguments:"
	puts "\t\t\[-cp conn pattern\] \[-sc scenario\] \[-nn nodes\]"
	puts "\t\t\[-seed seed\] \[-stop sec\] \[-tr tracefile\]\n"
}

proc getopt {argc argv} {
	global opt
	lappend optlist cp nn seed sc stop tr x y

	for {set i 0} {$i < $argc} {incr i} {
		set arg [lindex $argv $i]
		if {[string range $arg 0 0] != "-"} continue

		set name [string range $arg 1 end]
		set opt($name) [lindex $argv [expr $i+1]]
	}
}

proc log-movement {} {
    global logtimer ns_ ns

    set ns $ns_
    source ~/Downloads/ns-allinone-2.35/ns-2.35/tcl/mobility/timer.tcl
    Class LogTimer -superclass Timer
    LogTimer instproc timeout {} {
	global opt node_;
	for {set i 0} {$i < $opt(nn)} {incr i} {
	    $node_($i) log-movement
	}
	$self sched 0.1
    }

    set logtimer [new LogTimer]
    $logtimer sched 0.1
}

source ~/Downloads/ns-allinone-2.35/ns-2.35/tcl/lib/ns-bsnode.tcl
source ~/Downloads/ns-allinone-2.35/ns-2.35/tcl/mobility/com.tcl

getopt $argc $argv

if { $opt(x) == 0 || $opt(y) == 0 } {
	usage $argv0
	exit 1
}
if {$opt(seed) > 0} {
	puts "Seeding Random number generator with $opt(seed)\n"
	ns-random $opt(seed)
}

set ns_		[new Simulator]
set chan	[new $opt(chan)]
set prop	[new $opt(prop)]
set topo	[new Topography]

set tracefd   [open $opt(tr) w]
$ns_ trace-all  $tracefd

set namfile [open $opt(nam) w]
$ns_ namtrace-all-wireless $namfile $opt(x) $opt(y)

$topo load_flatgrid $opt(x) $opt(y)

$prop topography $topo

set god_ [create-god $opt(nn)]

$ns_ node-config -adhocRouting gpsr \
                 -llType $opt(ll) \
                 -macType $opt(mac) \
                 -ifqType $opt(ifq) \
                 -ifqLen $opt(ifqlen) \
                 -antType $opt(ant) \
                 -propType $opt(prop) \
                 -phyType $opt(netif) \
                 -channelType $opt(chan) \
                 -topoInstance $topo \
                 -agentTrace ON \
                 -routerTrace ON \
                 -macTrace OFF \
                 -movementTrace ON

source ./gpsr.tcl

for {set i 0} {$i < $opt(nn) } {incr i} {
    gpsr-create-mobile-node $i
}
# Define node initial position in nam

for {set i 0} {$i < $opt(nn)} {incr i} {

    # 20 defines the node size in nam, must adjust it according to your scenario
    # The function must be called after mobility model is defined
    
    $ns_ initial_node_pos $node_($i) 100
    $ns_ color $node_($i) blue
}

if { $opt(cp) == "" } {
	puts "*** NOTE: no connection pattern specified."
        set opt(cp) "none"
} else {
	puts "Loading connection pattern..."
	source $opt(cp)
}

if { $opt(sc) == "" } {
	puts "*** NOTE: no scenario file specified."
        set opt(sc) "none"
} else {
	puts "Loading scenario file..."
	source $opt(sc)
	puts "Load complete..."
}

for {set i 0} {$i < $opt(nn) } {incr i} {
    $ns_ at $opt(stop).000000001 "$node_($i) reset";
}

$ns_ at $opt(stop).00000001 "puts \"NS EXITING...\" ; $ns_ halt"

puts $tracefd "M 0.0 nn $opt(nn) x $opt(x) y $opt(y) rp $opt(rp)"
puts $tracefd "M 0.0 sc $opt(sc) cp $opt(cp) seed $opt(seed)"
puts $tracefd "M 0.0 prop $opt(prop) ant $opt(ant)"




puts "Starting Simulation..."
$ns_ run
