
# GPSR routing agent settings
for {set i 0} {$i < $opt(nn)} {incr i} {
    $ns_ at 0.00002 "$ragent_($i) turnon"
    $ns_ at 3.0 "$ragent_($i) neighborlist"
#    $ns_ at 149.0 "$ragent_($i) turnoff"
#	$ns_ at 80 "$ragent_($i) turnon"
}

#100,150,200,250
$ns_ at 3.0 "$ragent_(100) startSink 10.0" 
#$ns_ at 11.0 "$ragent_(9) startSink 0.0"


# GPSR routing agent dumps
#ns_ at 25.0 "$ragent_(51) sinklist"


# Upper layer agents/applications behavior
set null_(0) [new Agent/Null]
$ns_ attach-agent $node_(100) $null_(0)

set udp_(0) [new Agent/UDP]
$ns_ attach-agent $node_(101) $udp_(0)

set cbr_(0) [new Application/Traffic/CBR]
$cbr_(0) set packetSize_ 32
$cbr_(0) set interval_ 1.0
$cbr_(0) set random_ 1
#    $cbr_(0) set maxpkts_ 100
$cbr_(0) attach-agent $udp_(0)
$ns_ connect $udp_(0) $null_(0)
$ns_ at 10.0 "$cbr_(0) start"
$ns_ at 195.0 "$cbr_(0) stop" 

#set udp_(2) [new Agent/UDP]
#$ns_ attach-agent $node_(89) $udp_(2)

#set null_(2) [new Agent/Null]
#$ns_ attach-agent $node_(99) $null_(2)

#set cbr_(2) [new Application/Traffic/CBR]
#$cbr_(2) set packetSize_ 32
#$cbr_(2) set interval_ 2.0
#$cbr_(2) set random_ 1
#    $cbr_(2) set maxpkts_ 2
#$cbr_(2) attach-agent $udp_(2)
#$ns_ connect $udp_(2) $null_(2)
#$ns_ at 76.0 "$cbr_(2) start"
#$ns_ at 150.0 "$cbr_(2) stop"



#set udp_(3) [new Agent/UDP]
#$ns_ attach-agent $node_(51) $udp_(3)

#set null_(3) [new Agent/Null]
#$ns_ attach-agent $node_(99) $null_(3)

#set cbr_(3) [new Application/Traffic/CBR]
#$cbr_(3) set packetSize_ 32
#$cbr_(3) set interval_ 2.0
#$cbr_(3) set random_ 1
##    $cbr_(3) set maxpkts_ 2
#$cbr_(3) attach-agent $udp_(3)
#$ns_ connect $udp_(3) $null_(3)
#$ns_ at 76.0 "$cbr_(3) start"
#$ns_ at 150.0 "$cbr_(3) stop" 
