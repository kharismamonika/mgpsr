#
# nodes: 100, max time: 300.000000, max x: 200.00, max y: 200.00, chain: 10
# nominal range: 250.00 link bw: 2000000.00
# pause: 0.00, max speed: 0.00

$node_(0) set X_ 0.000000000000
$node_(0) set Y_ 100.000000000000
$node_(0) set Z_ 0.000000000000
$node_(1) set X_ 200.000000000000
$node_(1) set Y_ 100.000000000000
$node_(1) set Z_ 0.000000000000
$node_(2) set X_ 400.000000000000
$node_(2) set Y_ 100.000000000000
$node_(2) set Z_ 0.000000000000
$ns_ at 0.0 "$node_(0) setdest 0 101 0"
# $ns_ at 0.0 "$node_(1) setdest 200 101 0"
# $ns_ at 0.0 "$node_(2) setdest 400 101 0"
# $ns_ at 3.0 "$node_(0) setdest 0 102 0"
# $ns_ at 3.0 "$node_(1) setdest 200 102 0"
# $ns_ at 3.0 "$node_(2) setdest 400 102 0"
# $ns_ at 6.0 "$node_(0) setdest 0 103 0"
# $ns_ at 6.0 "$node_(1) setdest 200 103 0"
# $ns_ at 6.0 "$node_(2) setdest 400 103 0"