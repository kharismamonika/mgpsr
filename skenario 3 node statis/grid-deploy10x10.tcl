#
# nodes: 100, max time: 300.000000, max x: 200.00, max y: 200.00, chain: 10
# nominal range: 250.00 link bw: 2000000.00
# pause: 0.00, max speed: 0.00

$node_(0) set X_ 0.000000000000
$node_(0) set Y_ 100.000000000000
$node_(0) set Z_ 0.000000000000
$node_(1) set X_ 200.000000000000
$node_(1) set Y_ 100.000000000000
$node_(1) set Z_ 0.000000000000
$node_(2) set X_ 400.000000000000
$node_(2) set Y_ 100.000000000000
$node_(2) set Z_ 0.000000000000
